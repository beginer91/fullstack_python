salary = int(input("Введите зарплату за месяц: "))
ipoteka = int(input("Какой процент(%) от зп уходит на ипотеку? "))
live = int(input("Какой процент(%) от зп уходит 'на жизнь'? "))


def percentage(a, b):
  return float(b) * float(a)/100


print(f"На ипотеку было потрачено: {percentage(salary, ipoteka)*12} рублей") 
print(f"Было накоплено: {salary*12 - percentage(salary, ipoteka)*12 - percentage(salary, live)*12} рублей")

