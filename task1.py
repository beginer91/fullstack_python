len_kv = float(input("Введите длину стороны квадрата "))
perimetr_kvadrat = len_kv*4
area_kvadrat = len_kv**2
print(f' Периметр квадрата: {perimetr_kvadrat} \n Площадь квадрата: {area_kvadrat}')

len_rectangle = float(input("Введите длину стороны прямоугольника "))
width_rectangle = float(input("Введите ширину стороны прямоугольника "))
perimetr_rectangle = (len_rectangle + width_rectangle) *2
area_rectangle = width_rectangle * len_rectangle
print(f' Периметр прямоугольника: {perimetr_rectangle} \n Площадь прямоугольника: {area_rectangle}')
